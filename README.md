### Summary

Disable GNOME Captive Portal on Fedora Silverblue.

It's not tested on the standard Fedora Workstation and it'll probably break. I don't know how to properly replace files installed by other RPMs while still offering clean uninstallation. However since you get mutable filesystem you can just replace `/usr/libexec/gnome-shell-portal-helper` manually, this won't survive update though.

### Tutorial

To build RPM file, do the following:


Create toolbox:

```bash
toolbox create -y "rpmbuild"
```

Install required tools:

```bash
toolbox run -c "rpmbuild" sudo dnf install -y bubblewrap rpmdevtools
```

Clone this project:

```bash
git clone https://gitlab.com/thjderjktyrjkt/mask-gnome-shell-portal-helper.git
```

Go to the directory:

```bash
cd mask-gnome-shell-portal-helper
```

Start building in a bubblewrap sandbox, the current directory became the home directory:

```bash
toolbox run -c "rpmbuild" bwrap \
    --proc /proc \
    --ro-bind /usr /usr \
    --ro-bind /var/lib /var/lib \
    --ro-bind /etc /etc \
    --symlink usr/lib64 /lib64 \
    --symlink usr/bin /bin \
    --symlink usr/sbin /sbin \
    --bind . /home \
    --dev-bind /dev/null /dev/null \
    --tmpfs /var/tmp \
    --symlink var/tmp /tmp \
    --tmpfs /run/user \
    --unshare-all \
    --uid 1000 --gid 1000 \
    --hostname fedora \
    --clearenv \
    --setenv PATH /usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin \
    --setenv HOME /home \
    --setenv XDG_RUNTIME_DIR /run/user/1000 \
    --chdir /home \
    rpmbuild -ba mask-gnome-shell-portal-helper.spec
```

Install RPM:

```bash
rpm-ostree install ./rpmbuild/RPMS/noarch/mask-gnome-shell-portal-helper-1-1.noarch.rpm
```

---

### Backgrounds

GNOME Captive Portal opens up a embeded web browser utilizing WebKitGTK in the background automatically whenever NetworkManager detects the network requires sign-in.

However, this is both a security and privacy concern because:

- The window opens automatically, without user intervation
- JavaScript is enabled, and can't be disabled, this allows many attack vectors to be made possible:
  - gnome-shell-portal-helper doesn't seem to run sandboxed, so a compromised WebKitGTK means potentially compromised user account
  - Captive Portal relies on Man-in-the-Middle attack to function, an attacker could also use this to run arbitary JavaScript codes in user's system without user's awareness, as the window pops up in the background automatically
  - WebKitGTK wasn't made to be anti-fingerprint. A network router / MiM could fingerprint your device by running tracking code, thus rendering randomized MAC address entirely useless.
  - A mining script could also be delivered to the user, draining device's battery and worn out lifetime.

Windows and Android doesn't have this issue, as they both require user intervation to open Captive Portal page. Android also has Bromite WebView, further hardening the security and privacy.
