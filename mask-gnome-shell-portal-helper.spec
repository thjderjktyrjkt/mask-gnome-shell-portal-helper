Name:           mask-gnome-shell-portal-helper
Version:        1
Release:        1
Summary:        Disable Captive Portal on GNOME
Requires:       gnome-shell
License:        AGPL

BuildArch:      noarch

%description
Disable Captive Portal on GNOME (gnome-shell-portal-helper)

%prep
%setup -q -c -T


%build
cat > gnome-shell-portal-helper.mask <<EOF
#!/bin/sh
# Stub file for gnome-shell-portal-helper
# Disables Captive Portal on GNOME
return 0
EOF


%install
mkdir -p %{buildroot}/usr/libexec
install --preserve-timestamps --mode=755 \
    gnome-shell-portal-helper.mask \
    %{buildroot}/usr/libexec/gnome-shell-portal-helper.mask
%pre

%post
mv --no-clobber \
    /usr/libexec/gnome-shell-portal-helper \
    /usr/libexec/gnome-shell-portal-helper.original
install --preserve-timestamps --mode=755 \
    /usr/libexec/gnome-shell-portal-helper.mask \
    /usr/libexec/gnome-shell-portal-helper

%preun

%postun
mv --force \
    /usr/libexec/gnome-shell-portal-helper.original \
    /usr/libexec/gnome-shell-portal-helper

%files
%attr(0755,root,root) /usr/libexec/gnome-shell-portal-helper.mask

%changelog
# Nothing
